/**
 *
 * WIN needs to be linked with -lshlwapi
 *
 */

#include "osdir.h"

/**
 * WINDOWS SPECIFIC CODE!
 */
#ifdef __WIN32
#include <windows.h>
#include <tchar.h>
#include <Shlwapi.h>
char dir_sep(){
    return '\\';
}

std::string _getHomeDir(){
	std::string s = getenv("HOMEPATH");
    return s;
}

std::string _getExecPath(){
	WCHAR path[MAX_PATH];
	HMODULE hModule = GetModuleHandleW(NULL);
	GetModuleFileNameW(hModule, path, MAX_PATH);
	char ch[300];
	char defChar = ' ';
	WideCharToMultiByte(CP_ACP, 0, path, -1, ch, 300, &defChar, NULL);
	std::string s = ch;
	return _shorten(dir_sep(), s);
}

std::string _getAppDataFolder(){
	TCHAR szPath[MAX_PATH];
	SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA, NULL, 0, szPath);
	std::string s = szPath;
	return s;
}

bool _createDirectory(std::string path){
	return CreateDirectory(path.c_str(), NULL);
}


bool _directoryExists(std::string path){
	std::cout << path << std::endl;
	return (PathFileExists(path.c_str())) ? true : false;
}

/** LINUX SPECIFIC CODE */
#elif __linux
#include <unistd.h>
#include <sys/stat.h>



char dir_sep(){
    return '/';
}



std::string _getHomeDir(){
    std::string s = getenv("HOME");
    return s;
}

std::string _getExecPath(){
    std::string s;
    char buf[1024];
    ssize_t len = ::readlink("/proc/self/exe", buf, sizeof(buf));
    if(len != -1){
        buf[len] = '\0';
        s = buf;
        return _shorten(dir_sep(), s);

    }
    else {
        std::cout << "\nError locating execution path\n";
        exit(1);
    }
}

bool _directoryExists(std::string path){
    struct stat sb;
    if(stat(path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)){
        return true;
    }
    else {
        return false;
    }
}


bool _createDirectory(std::string path){
    int i = mkdir(
        path.c_str(),
        S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH
    );
    return (i != 0 ) ? false : true;
}

std::string _getAppDataFolder(){
    std::string s;
    s = _getHomeDir() + dir_sep()
        + ".local" + dir_sep() + "share";
    return s;
}



/**
 *  MACOSX SPECIFIC CODE
 */
#elif __APPLE__
#include "TargetConditionals.h"
#if TARGET_OS_MAC
#include <mach-o/dyld.h>
#include <unistd.h>
#include <sys/stat.h>
#include <CoreServices/CoreServices.h>

char dir_sep(){
    return '/';
}

std::string _getHomeDir(){
    std::string s = getenv("HOME");
    return s;
}

std::string _getExecPath(){
    char path[1024];
    uint32_t size = sizeof(path);
    std::string s;
    if(_NSGetExecutablePath(path, &size) == 0){
        s = path;
        s = _shorten(dir_sep(), s);
        s = _shorten(dir_sep(), s);
        return s;
    }
    else {
        return s;
    }
}

bool _createDirectory(std::string path){
    int i = mkdir(
        path.c_str(),
        S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH
    );
    return (i != 0 ) ? false : true;
}

bool _directoryExists(std::string path){
    struct stat sb;
    if(stat(path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)){
        return true;
    }
    else {
        return false;
    }
}

std::string _getAppDataFolder(){
    return _getHomeDir() + dir_sep() + "Library" + dir_sep() + "Application Support";
}

#endif

/*
 *  MISC PLATFORM
 *
 */
#else
char dir_sep(){
    notSupported();
}

std::string _getHomeDir(){
    notSupported();
}

std::string _getExecPath(){
    notSupported();
}

bool _createDirectory(std::string path){
    notSupported();
}

bool _directoryExists(std::string path){
    notSupported()
}

std::string _getAppDataFolder(){
    notSupported();
}

void notSupported(){
    std::cout << "This platform is not supported at the moment\n";
    exit(1);
}

#endif

/**
 * ALL OS'S
 *
 */
std::string _shorten(char delimiter, std::string str){
    const char* oldCharString = str.c_str();
    std::string newString;
    int len = strlen(oldCharString);
    int currPos = 0;
    for(int i = len; i > 0; i--){
        if(oldCharString[i] == dir_sep()){
            currPos = i;
            break;
        }
    }
    for(int i = 0; i < len; i++){
        if(i < currPos){
            newString += oldCharString[i];
        }
    }
    return newString;
}


/** Implementation CODE */

std::string OSDir::getHomeDir(){
    return _getHomeDir() + dir_sep();
}

char OSDir::dirsep(){
    return dir_sep();
}

std::string OSDir::getExecPath(){
    return _getExecPath() + dir_sep();
}

bool OSDir::createDirectory(std::string path){
	return _createDirectory(path);
}

bool OSDir::directoryExists(std::string path){
	return _directoryExists(path);
}

bool OSDir::createPath(std::string path){
    std::vector <std::string> vpath = split(path, dir_sep());
    std::string path_part;
    for(auto it = vpath.begin();
        it != vpath.end();
        it++){
        path_part += *it + dir_sep();
        if(!OSDir::directoryExists(path_part)){
            OSDir::createDirectory(path_part);
        }
    }
    return OSDir::directoryExists(path_part);
}



std::string OSDir::getAppDataFolder(){
	return _getAppDataFolder() + dir_sep();
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems){
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
