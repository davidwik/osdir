# OSDir #

A helper class with static functions that wraps OS depended features. 

Compiling with windows? Link with **-lshlwapi**

A compiler that support C++11 is needed. 



```
#!c++

char dir_sep()                           // Returns the directory separator for the build target / or \

string OSDir::getExecPath()              // Returns the full path to the binary's directory
string OSDir::getHomeDir()               // Returns the full path to the user's home directory
string OSDir::getAppDataFolder()         // Returns the full path to the App folder (%AppData%/local for windows)

bool OSDir::createDirectory(string)      // Creates a directory returns true if succeded
bool OSDir::createPath(string)           // Creates a full path and subdirs
bool OSDir::directoryExists(string)      // Returns true if a directory exists

```